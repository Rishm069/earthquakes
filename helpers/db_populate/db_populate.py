import hashlib

from pymongo import MongoClient
client = MongoClient()
client = MongoClient('mongodb', 27017)
db = client.earthquakes
collection = db.events

def populate():
    with open('/app/helpers/db_populate/events_list.csv') as file:
        for line in file:
            line = line.rstrip()
            date, time, lat, lon, mag = line.split(',')
            event_hash = hashlib.sha1(f"{date} {time} {lat} {lon} {mag}".encode('utf-8')).hexdigest()

            event = {
                'event_hash': event_hash,
                'date': date,
                'time': time,
                'lat': lat,
                'lon': lon,
                'mag': mag,
                }
            try:
                event_id = collection.insert_one(event).inserted_id
            except Exception as e:
                print(f'ERROR: {e}')
