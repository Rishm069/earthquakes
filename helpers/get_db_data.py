import pandas as pd
from helpers.data_retrival import fetch_data_updater
from helpers.db_populate.db_populate import populate
from pymongo import MongoClient

client = MongoClient()
client = MongoClient('mongodb', 27017)
db = client.earthquakes
collection = db.events


def get_db_data(year=None, latest_event=False):
    
    check_dbdata()
    
    #retrive data from DB
    events = collection.find()
    #convert to dataframe
    df = pd.DataFrame(list(events))
    #remove service columns
    del df['event_hash']
    del df['_id']
    #latitude, longitude and magnitude conversion to numeric     
    df['lat'] = pd.to_numeric(df['lat'])
    df['lon'] = pd.to_numeric(df['lon'])
    df['mag'] = pd.to_numeric(df['mag'])
    #sorting (from oldest to newest) and setting russian names
    df = df.sort_values(by=['date', 'time']).rename(columns={'date': 'Дата',
                                                             'time': 'Время',
                                                             'lat': 'Широта',
                                                             'lon': 'Долгота',
                                                             'mag': 'Магнитуда'})
    #reordering columns       
    df = df[['Дата', 'Время', 'Широта', 'Долгота', 'Магнитуда']]
    
    #filter by year
    if year != None:
        df = df[df['Дата'].str.contains(year)]
    
    #latest event filter
    if latest_event is True:
        df = df.tail(1)
    
    #ensure dropping duplicates
    df.drop_duplicates(keep="first", inplace=True)

    return df

def check_dbdata():
    if collection.count() == 0:
        populate()
