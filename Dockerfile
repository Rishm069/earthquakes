FROM python:3.7-slim

RUN mkdir app

WORKDIR /app

ADD . /app

RUN pip install -r requirements.txt

EXPOSE 5000
#EXPOSE 8050

CMD bash -c "nohup python updater.py > updater.log & gunicorn app:server --bind 0.0.0.0:5000"
#CMD bash -c "nohup python updater.py > updater.log & python app.py"
