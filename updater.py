import hashlib
import os
import schedule
import time
import logging
from helpers.data_retrival import fetch_data_updater
from pymongo import MongoClient
client = MongoClient()
client = MongoClient('mongodb', 27017)
db = client.earthquakes
collection = db.events

LOGLEVEL = os.environ.get('LOGLEVEL')

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=LOGLEVEL,
    datefmt='%Y-%m-%d %H:%M:%S')

event_id = None

def job():
    current_events = fetch_data_updater()
    for event in current_events:
        date, time, lat, lon, mag = event
        event_hash = hashlib.sha1(f"{date} {time} {lat} {lon} {mag}".encode('utf-8')).hexdigest()
        event = {
            'event_hash': event_hash,
            'date': date,
            'time': time,
            'lat': lat,
            'lon': lon,
            'mag': mag,
            }

        logging.info(f'INSERTING: {event}')
        if collection.count_documents({ 'event_hash': event_hash}):  
            logging.info(f'EVENT {event} ALREADY EXISTS')
        else:    
            try:
                event_id = collection.insert_one(event).inserted_id
                logging.info(f'SUCCESFULLY INSERTED: {event}')
            except Exception as e:
                logging.error(f'ERROR: {e}')

schedule.every(1).minutes.do(job)
# schedule.every().hour.do(job)
# schedule.every().day.at("10:30").do(job)
# schedule.every().monday.do(job)
# schedule.every().wednesday.at("13:15").do(job)
# schedule.every().minute.at(":17").do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
