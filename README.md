# earthquakes38

Simple Dash Plotly application with actual and historical data about earthquakes in Baikalia region.
Available at: earthquakes38.herokuapp.com

All data received from seis-bykl.ru

Verison: Version: 1.2.1

## Requirements

* Python > 3.7

docker and docker-compose

* gunicorn==20.0.4
* requests==2.25.1
* dash==1.19.0
* pandas==1.2.3
* dash_bootstrap_components==0.11.3
* schedule==1.0.0
* pymongo==3.11.3

### How to run locally

Use docker-compose to start application interactively:

```bash
make up
```

Note: Database is automatically populated with historical data (1994-2020).

### How database is updated

The data is being scrapped from seis-bykl.ru directly every one minute (via schedule in `updater.py`) into `earthquakes` database, events `collection`.

### DB schema

event_hash: string
date: string
time: string
lat: string
lon: string
mag: string

Note: event_hash is sha1 hash of the whole event string (`"{date} {time} {lat} {lon} {mag}"`)

### Logging

`updater.py` checks `LOGLEVEL` env variable and sets loglevel accordingly.

Updater logs are put into `/app/updater.log`

Unicorn sends logs to Docker.
