APP_VERSION ?= 1.2.1
IMAGE_TAG ?= ${APP_VERSION}
IMAGE_NAME = rishm/earthquakes

build-release:	
	docker build -f Dockerfile . -t ${IMAGE_NAME}:${IMAGE_TAG} --compress

push-release: 
	docker push ${IMAGE_NAME}:${IMAGE_TAG}

up:
	docker-compose up

down:
	docker-compose down